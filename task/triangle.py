def is_triangle(a, b, c):
    '''
    please add your solution here or call your solution implemented in different function from here  
    then change return value from 'False' to value that will be returned by your solution
    '''
    if not a + b > c:
        return False
    
    if not a + c > b:
        return False

    if not b + c > a:
        return False
    
    return True
